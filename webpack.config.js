'use strict';
var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: './src/App.jsx',
    output: { path: __dirname + '/public', filename: 'bundle.js' },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            },{
                test: /\.css$/,
                loaders: ['style', 'css']
            }
        ]
    },
};                
