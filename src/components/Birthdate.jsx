'use strict';

import React, { Component } from 'react'
import Select from './Select.jsx'
import Input from './Input.jsx'

const Months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Sepetember', 'October', 'November', 'December'];

const Days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];

class BirthDate extends Component {
    render() {
        return(
            <div>
                <p style={{fontSize: '24px', color: '#6f788c'}}>When were you born?</p>
                <Select options={Months}
                        default="Birth Month"
                        update={this.props.update}
                        name="birthMonth"
                        label="Birth month"/>
                <Select options={Days}
                        default="Birth Date"
                        update={this.props.update}
                        name="birthDay"
                        label="Birth day"/>
                <Input type="number"
                       update={this.props.update}
                       placeholder="Birth year?"
                       name="birthYear"
                       label="Birth year"
                    />
            </div>
        )
    } 
}

export default BirthDate;
