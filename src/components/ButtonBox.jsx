'use strict';

import React, { Component } from 'react';
import ButtonBoxSelect from './ButtonBoxSelect.jsx';

let styles = {
    default: {
        margin: '0 auto',
    }
};

class ButtonBox extends Component {
    render() {
        return (
            <div style={styles.default}>
                                <p style={{fontSize: '24px', color: '#6f788c'}}>{this.props.prompt}</p>
                    {this.props.text.map((el, i) => {
                        return(<ButtonBoxSelect graphic={this.props.graphic}
                                                name={this.props.name}
                                                text={el}
                                                key={this.id}
                                                handleButtonClick={this.props.handleButtonClick}
                                                value={this.props.value[i]} />)})}
            </div>
        )
    }
};

export default ButtonBox;
