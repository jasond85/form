'use strict';

import React, { Component } from 'react'
import Radium from 'radium'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import ResultsWindow from './ResultsWindow.jsx'
import Slider from './Slider.jsx'
import '../styles/bootstrapgrid.css'

const style = {
    wrapper: {
        minHeight: '100vh',
        width: '100vw',
        backgroundColor: '#fff',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    infoBar: {
        backgroundColor: '#ffaf10',
        width: '100%',
        paddingLeft: '15px',
    },
    infoBarText:{
        fontSize: '18px',
        color: '#fff',
        marginTop: 0,
        paddingTop: '10px',
        display: 'inline-block',
    }
};


class Wrapper extends Component {
    
    render() {
        return (
            <ReactCSSTransitionGroup 
                            transitionName="appearFormSection" 
                            transitionAppear={true} 
                            transitionAppearTimeout={400}>
                <div style={style.wrapper}>
                    <div style={style.infoBar}>
                        <p style={style.infoBarText}>
                            LifeQuote: {this.props.progress()}% to top term life insurance quotes
                        </p>
                    </div>
                    <div style={{backgroundColor: "#fff"}}>
                        <Slider min="10" 
                                max="30" 
                                step="5"
                                name="yearsCoverage"
                                update={this.props.update} 
                                prompt="Change term length"/>
                                
                        <Slider min="50000"
                                max="5000000"
                                step="50000"
                                name="coverageAmount"
                                update={this.props.update} 
                                prompt="Change coverage amount"/>

                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                {this.props.children}
                            </div>   
                            <div className="col-md-6">
                                <ResultsWindow progress={this.props.progress} />
                            </div>    
                        </div>
                    </div>
                </div>        
            </ReactCSSTransitionGroup>
        );
    }
};


export default Radium(Wrapper)
