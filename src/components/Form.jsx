'use strict';

import React from 'react'
import ReactDOM from 'react-dom'
import PersonalDetails from './PersonalDetails.jsx'
import Wrapper from './Wrapper.jsx'
import HealthQuestions from './HealthQuestions.jsx'
import InsuranceQuestions from './InsurnaceQuestions.jsx'
import BirthDate from './BirthDate.jsx'
import QuoteList from './QuoteList.jsx'
import ButtonBox from './ButtonBox.jsx'
import Button from './Button.jsx'
import '../styles/animations.css'
import '../styles/main.css'


const Form = React.createClass({    
    getInitialState() {
        return {
            address: null,
            firstName: null,
            lastName: null,
            birthMonth: null,
            birthDay: null,
            birthYear: null,
            height: null,  
            weightInPounds: null,
            city: null,
            state: null,
            zipCode: null,
            phone: null,
            phoneType: null,
            email: null,
            timeToCall: null,
            gender: null,
            tobaccoUse: null,
            prescripionMedUse: null,
            coverageAmount: null,
            yearCoverage: null,
            agreeToTerms: null,
            step: 1
        }
    },

    nextStep() {
        this.setState({
            step: this.state.step + 1
        });
    },

    handleInput(event){
        // get value from form input value 
        // or div input as first child of containing div 
        var value =  event.target.value || event.target.firstChild.value;
        var name = event.target.name || event.target.id;
        this.setState({[name]: value})
     },

     handleButtonBoxClick(event) {
        this.handleInput(event);
        this.progress();
        this.nextStep();
     },

     progress()  {
             // used for percentage progress indicator
             let counter = 0;
             const total = 18;
             for(var value in this.state) {
                !!this.state[value] ? counter+=1 : null;
             } 
             return (Math.ceil(counter/total * 100));
    },


     render() {
            switch(this.state.step) {
                case 1: 
                    return (
                        <div>
                            <InsuranceQuestions  handleButtonClick={this.nextStep} 
                                                nextStep={this.nextStep}
                                                update={this.handleInput}/>
                        </div>    
                            )
                    break;
                case 2: 
                    return (
                            <Wrapper progress={this.progress}
                                update={this.handleInput}
                                checkValue={this.checkValue}>
                                <ButtonBox prompt="Your gender:" 
                                    text={["Male", "Female"]} 
                                    value={["male", "female"]}
                                    name="gender"
                                    handleButtonClick={this.handleButtonBoxClick} />
                            </Wrapper>
                        );
                        break;
                case 3: 
                    return ( 
                        <Wrapper progress={this.progress}> 
                                <ButtonBox prompt="Do you use tobacco products?" 
                                    text={['Yes', 'No']} 
                                    value={["yes", "no"]}
                                    name="tobaccoUse"
                                    handleButtonClick={this.handleButtonBoxClick}/>
                        </Wrapper>

                    );
                    
                case 4:
                    return (
                      <Wrapper progress={this.progress}>
                                <ButtonBox prompt="Do you take presecription medications?" 
                                    text={['Yes', 'No']} 
                                    value={["yes", "no"]}
                                    name="prescriptionMedUse"
                                    handleButtonClick={this.handleButtonBoxClick}/>
                    </Wrapper>)
                case 5:
                    return (
                        <Wrapper progress={this.progress}>
                            <HealthQuestions 
                                    update={this.handleInput}
                                    nextStep={this.nextStep}/>
                        </Wrapper>
                    )
                    break;
                case 6: 
                    return (
                        <Wrapper progress={this.progress}>
                            <BirthDate update={this.handleInput} />
                            <Button text="Next" handleButtonClick={this.nextStep}/>
                        </Wrapper>
                    );
                    break;
                case 7:
                    return(
                        <Wrapper progress={this.progress}>
                            <PersonalDetails     
                                        nextStep={this.nextStep}
                                        update={this.handleInput} />
                        </Wrapper>
                    )
                    break;
                case 8: 
                    return <QuoteList />
         }
    }
});

module.exports = Form; 
