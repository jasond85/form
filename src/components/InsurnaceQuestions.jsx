import React, { Component } from 'react'
import Button from './Button.jsx'
import Select from './Select.jsx'

const coverageAmounts = [
    50000, 75000, 100000, 125000, 150000,175000,200000,225000, 250000, 275000, 300000, 325000,
    350000, 375000, 400000, 425000, 450000, 475000, 500000, 550000, 600000, 650000, 700000, 750000, 800000,
    850000, 900000, 950000, 1000000, 1250000, 1500000, 1750000, 2000000, 2250000, 2500000, 2750000, 3000000,
    3250000, 3500000, 3750000, 4000000, 4250000, 4500000, 4750000, 5000000
];

const yearsCoverage = [10, 15, 25, 30];

class InsuranceQuestions extends Component {

    render() {

        let formattedCoverageAmounts = coverageAmounts.map(amount => `$${amount}`);
        let formattedYearsCoverage = yearsCoverage.map(years => `${years} years`);

        return (
            <div style={{margin: '0 auto'}}>
                <Select options={formattedCoverageAmounts}
                        default="Choose coverage amount" 
                        update={this.props.update} 
                        name="coverageAmount"/>
                <Select options={formattedYearsCoverage} 
                        default={"Choose term length"}
                        update={this.props.update} 
                        name="yearsCoverage"/>
                <Button text="Next" handleButtonClick={this.props.handleButtonClick}/>
            </div>
        );
    }
};

export default InsuranceQuestions;
