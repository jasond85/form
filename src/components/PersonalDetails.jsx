"use strict";

import React, { Component } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import Button from './Button.jsx'
import Input from './Input.jsx'
import Select from './Select.jsx'
import '../styles/animations.css'

const STATES = [
  'AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI',
  'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS',
  'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR',
  'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'
];

class PersonalDetails extends Component{
    render(){
        return (
            <ReactCSSTransitionGroup transitionName="appearFormSection"
                transitionAppear={true}
                transitionAppearTimeout={500}>
                <p style={{fontSize: '24px', color: '#6f788c'}}>Last section:</p>
                <form>
                    <Input type="text" 
                        udpdate={this.props.update} 
                        placeholder="First Name" 
                        name="firstName"
                        label="First name"/>
                    <Input type="text
                        "update={this.props.update} 
                        placeholder="Last Name" 
                        name="lastName"
                        label="Last name"/>
                    <Input type="text" 
                        update={this.props.update} 
                        placeholder="Street Address" 
                        name="address"
                        label="Street address"/>
                    <Input type="text"update={this.props.update} 
                        placeholder="City" 
                        name="city"
                        label="City"/>
                   <Select update={this.props.update} 
                           default="State:"
                           options={STATES} 
                           name="state"
                           label="State"/>
                   <Input type="number" 
                           update={this.props.update} 
                           placeholder="Zip Code" 
                           name="zipCode"
                           label="Zipcode"/>
                    <Input type="tel" 
                           update={this.props.update} 
                           placeholder="Primary Phone" 
                           name="phone"
                           label="Phone number"/>
                    <Input type="email" 
                        update={this.props.update} 
                        placeholder="Email" 
                        name="email" 
                        label="Email"/>
               </form>
               <Button text="Finish"/>
            </ReactCSSTransitionGroup>       
        )
    }
};

export default PersonalDetails;
