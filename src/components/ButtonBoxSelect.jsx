'use strict';
import Radium from 'radium'
import React, { Component } from 'react';

let styles = {
    default: {
        border: '1px solid #fff',
        color: '#6f788c',
        marginRight: '10px',
        borderRadius: '2%',
        boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        height: '200px',
        width: '200px',
        backgroundColor: '#fff',
        display: 'inline-block',
        cursor: 'pointer',
        ':hover': {
            boxShadow: '0 10px 20px rgba(0,0,0, 0.15), 0 6px 6px rgba(0, 0, 0, 0.23)',
            transition: 'all 0.3s ease-in'

        }
    },
    selected: {
        backgroundColor: '#063454'
    },
    text: {
        textAlign: 'center',
        fontSize: '24px'
    }
};

class ButtonBoxSelect extends Component {

    
    render() {
   
        var computeStyle = function() {
            return styles.default;
            this.state[this.props.name]  ? styles.selected : styles.default;
        }
        return(
            <div id={this.props.name}
                 onClick={this.props.handleButtonClick}   
                 style={computeStyle()}
                 value={this.props.value}> 
                <input type='hidden' value={this.props.value}></input> 
                <p style={styles.text}>{this.props.text}</p>
                <img src={this.props.graphic} />
            </div>
        )
    }
}

export default Radium(ButtonBoxSelect); 
