'use strict';

import Radium from 'radium'
import React, { Component } from 'react'

const styles = {
    input: {
        fontSize: '18px',
        paddingTop: '10px',
        paddingBottom: '10px',
        border: '1px solid #fff',
        boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        borderRadius: '2px',
        height: '44px',
        minWidth: '150px',
        backgroundColor: '#fff',
    },
    label: {
        fontSize: '12px',
        color: '#6f788c',
    }
};

class Select extends Component {   
    render() {
        return (
            <div>
                {(() => {
                    if (this.props.label) {
                     return(   <label style={styles.label} for={this.props.name}>
                            <p>{this.props.label}</p>
                        </label>);
                    }
                    return;
                })()}
                <select style={styles.input} 
                    onChange={this.props.update}
                    placeholder={this.props.placeholder}
                    name={this.props.name}>
                    {
                        (()=>{
                            if(this.props.default) {
                                return (<option disabled selected 
                                    key={this.props.default.id}>
                                    {this.props.default}
                                    </option>);
                            }
                        })()
                    }
                    {
                        this.props.options.map((option, index ) => 
                        <option value={option} 
                            key={index.id}>
                            {this.props.options[index]}
                        </option>
                        
                      )
                    }
                </select>
            </div>
        )
    }
};

export default Radium(Select) 
