'use strict';

import React, { Component } from 'react';
import Radium from 'radium'
//import '../styles/slider.css'

const style = {
    wrapper: {
        paddingLeft: '25px'
    },
    slider: {
        display: 'inline-block',
        width: '20%'
    },
    computedValue: {
        display: 'inline-block',
        marginLeft: '15px',
        fontSize: '1.4em',
        color: '#778899'
    },
    prompt: {
        fontSize: '1.2em',
        marginTop: 0,
        marginBottom: '-15px',
    }
}

class Slider extends Component {
    
  constructor(props) {
        super(props);
        this.state = {[this.props.name]: ''};
  } 
  
  setValue(event) {
        this.setState({[event.target.name]: event.target.value});
  }

    render() {
        return (
            <div style={style.wrapper}>
                <p style={style.prompt}>{this.props.prompt} </p>
                <input type="range"
                       style={style.slider}
                       name={this.props.name} 
                       onInput={this.props.update}
                       onChange={this.setValue.bind(this)}
                       step={this.props.step}
                       min={this.props.min}
                       max={this.props.max}
                       maxWidth="300px"></input>
                <p style={style.computedValue}>{this.state[this.props.name]}</p>
            </div>
        )
    }
};

export default Radium(Slider);
