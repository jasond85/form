'use strict';

import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Select from './Select.jsx';
import Input from './Input.jsx';
import Button from './Button.jsx';
import ButtonBox from './ButtonBox.jsx';
import '../styles/animations.css';

const HEIGHTS = [
    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 410, 411, 50, 51, 52, 
    53, 54, 55, 56, 57, 58, 59, 510, 511, 60, 61, 62, 63, 64, 65, 66, 67, 68, 
    69, 610, 611, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 710, 711
];

class HealthQuestions extends Component {
    render() {
        
        let formattedHEIGHTS = HEIGHTS.map(function(height){ 
            return `${height.toString()[0]}' ${height.toString().slice(1)}''`
        });

        return (
            <ReactCSSTransitionGroup 
                transitionName="appearFormSection" 
                transitionAppear={true} 
                transitionAppearTimeout={500}>

                <p style={{color: '#6f788c', fontSize: '24px'}}>Health Questions</p>
                <Input update={this.props.update} 
                    placeholder="Your weight in pounds" 
                    type="number"
                    name="weightInPounds"
                    label="Body weight:"/>
                <Select update={this.props.update} 
                           options={formattedHEIGHTS} 
                           default="Height"
                           value={HEIGHTS}
                           name="height"
                           key={HEIGHTS}
                           label="Height"/> 
                <Button text="Next" handleButtonClick={this.props.nextStep} key="sdf" />
            </ReactCSSTransitionGroup>
        );
    }    
};

export default HealthQuestions
