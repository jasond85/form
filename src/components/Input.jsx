import React from 'react'


var styles = {
    input: {
        fontSize: '18px',
        boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        padding: '10px',
        border: '1px solid #fff',
        borderRadius: '2px'
    },
    label: {
        color: '#6f788c',
        fontSize: '12px',
    }
};

var Input = React.createClass({

    render() {
        return (
            <div>
                {(() => {
                    if (this.props.label) {
                     return(   <label style={styles.label} for={this.props.name}>
                            <p>{this.props.label}</p>
                        </label>)
                    }
                })()}
                <input style={styles.input}
                    type={this.props.type}
                    onChange={this.props.update} 
                    placeholder={this.props.placeholder}
                    name={this.props.name}
                    type={this.props.type}>
                </input>
            </div>
        )
    }
});

module.exports = Input;
