"use strict";
import React from 'react'

var styles = {
        fontSize: '18px',
        color: '#fff',
        backgroundColor: '#2ea1ff',
        borderColor: '#ff5a5f',
        borderBottomColor: '#e00007',
        boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        paddingTop: '10px',
        paddingBottom: '10px',
        borderRadius: '2px',
        width: '100px',
        textAlign:'center',
        display: 'inline-block',
        cursor: 'pointer'
};

var Button = React.createClass({

    render() {
        return(
            <div onClick={this.props.handleButtonClick}style={styles}>{this.props.text}</div>
        )
    }
});

module.exports = Button
