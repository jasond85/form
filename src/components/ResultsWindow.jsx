import React, { Component } from 'react'

const style = {
    progressText: {
        fontSize: '400%',
        textAlign: 'center',
        color: '#6f788c'
    }
}

class ResultsWindow extends Component {
    render(){
        return(
                <div>
                    <p style={style.progressText}>{this.props.progress()}% complete</p>
                </div>
        )
    }
};

export default ResultsWindow
