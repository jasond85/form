import React from 'react'
import ReactDOM from 'react-dom'
import Form from './components/Form.jsx'

var App = React.createClass({

    render() {
        return <Form />
    }
});

ReactDOM.render(<App />, document.getElementById('react-mount'));

