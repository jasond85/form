// store (state + reducers + dispatch)
// reducers connect actions to store

import { createStore } from 'redux'

let store = createStore(form);    


getInitialState() {
        return {
            address: null,
            firstName: null,
            middleName: null,
            lastName: null,
            birthMonth: null,
            birthDay: null,
            birthYear: null,
            height: null,  
            weightInPounds: null,
            city: null,
            state: null,
            zipCode: null,
            phone: null,
            phoneType: null,
            email: null,
            timeToCall: null,
            gender: null,
            tobaccoUse: null,
            prescripionMedUse: null,
            coverageAmount: null,
            yearCoverage: null,
            agreeToTerms: null,
            step: 1
    }
};


// redux actions
var actions = {
    updateFormItemState: function(event){
        // get value from form input value 
        // or div input as first child of containing div 
        var value =  event.target.value || event.target.firstChild.value;
        var name = event.target.name || event.target.id;
        console.log(name);
        console.log(value)
        this.setState({[name]: value})

    },
    nextStep: function(){
        this.setState({step: this.state.step + 1});
    }
};

module.exports = actions;

